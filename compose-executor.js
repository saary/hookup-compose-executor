//
// Docker executor
//
// The docker executor restarts the deployed repo in a dedicated docker container according to repo parameters

const util = require('util');
const { exec, execSync } = require('child_process');
const path = require('path');
const { exists, readFile } = require('fs');

const hostnameFile = process.env.HOOKUP_DOCKER_HOSTNAME_FILE || '/etc/docker-hostname';

const tryExecSync = (command, options) => {
  console.log(`Trying ${command}`);
  try {
    const output = execSync(command, options);
    if (output) console.log(output.toString());
  }
  catch(e) {
    console.error(`Failed to run: ${command}`, e);
  }
}

function dockerInspect({nameOrHash, varName }) {
  return new Promise((resolve, reject) => {
    exec(`docker inspect -f "{{json .Config.Env }} ${nameOrHash}"`, (error, stdout, stderr) => {
      if (error) {
        const err = new Error(`Docker inspect error: ${error}`);
        console.error(err.message);
        return reject(err);
      }
  
      let env;
      try {
        env = JSON.parse(stdout);
      }
      catch(e) {
        const err = new Error(`Failed to parse inspect result: ${stdout}`);
        console.error(err.message);
        return d.reject(err);
      }
  
      let vars = {};
      env.forEach((item) =>  {
        const parts = item.split('=');
        vars[parts[0]] = parts[1];
      });
  
      if (varName) {
        return resolve(vars[varName]);
      }
  
      resolve(vars);
    });
  });
}

exports.preDeploy = function(repo, cb) {
  return cb && cb();
};

exports.start = function(repo, username, branch) {
};

exports.restart = function(repo, username, branch, hash) {
  const normalizedDeployPath = repo.deployPath.replace(/\\/g, '/');
  const packageInstaller = repo.packageInstaller || 'yarn';    
  
  console.log(`[${repo.name || repo.workdir}] Installing packages with ${packageInstaller}`);
  exec(`${packageInstaller} install`, { cwd: normalizedDeployPath }, (err, stdout, stderr) => {
    if (err) console.error(`Error running ${packageInstaller}`, err);
    if (stderr) console.error('STDERR >>>', stderr);

    const nativeModuleNames = (repo.env && repo.env.HOOKUP_NATIVE_MODULES && repo.env.HOOKUP_NATIVE_MODULES.split(',')) || [];

    nativeModuleNames.forEach((nativeModule) => {
      const nativePackagesInstallCheckCommand = `cat package.json | grep "${nativeModule}" && (node -e "require('${nativeModule}'); console.log('${nativeModule} ok')" || npm rebuild ${nativeModule} --build-from-source)`
      try {
        console.log(`Rebuilding ${nativeModule} if necessary`);

        const output = execSync(nativePackagesInstallCheckCommand, { cwd: normalizedDeployPath });
        if (output) console.log(output.toString());
      }
      catch (e) {
        console.error(`Failed to run ${nativePackagesInstallCheckCommand}\n`, e);
      }
    })    

    const restart = () => {
      console.log(`Restarting container ${repo.name || repo.workdir}`);
      console.log(`[${repo.name || repo.workdir}] Restarting`);      
      exec(`docker restart ${repo.containerId}`, (err, stdout, stderr) => {
        if (err) {
          console.error('Error restarting', err);
        }
        if (stderr) {
          return console.error('STDERR >>>', stderr);        
        }
        
        console.log(`[${repo.name || repo.workdir}] Restarted - `, stdout);
        if (process.env.HOOKUP_POST_RESTART_COMMAND) {
          exec(process.env.HOOKUP_POST_RESTART_COMMAND);
        }
      })
    };

    const restartService = () => {
      console.log(`Restarting service ${repo.serviceId}`);
      console.log(`[${repo.name || repo.workdir}] Checking if lowest instance for ${repo.serviceId}`);
      exec(`docker service ps ${repo.serviceId} -f desired-state=running | cut -f3,7 -d' ' | sed '/^\s*$/d'`, (err, stdout, stderr) => {
        if (err) {
          console.error('Error restarting', err);
        }
        if (stderr) {
          return console.error('STDERR >>>', stderr);        
        }

        const lines = stdout.split('\n').filter(line => line && line.trim().length);
        const instances = [];
        lines.forEach((line) => {
          const parts = line.split(' ');
          instances.push({ name: parts[0], node: parts[1] });
        });

        instances.sort((a,b) => a.name.localeCompare(b.name));
        exists(hostnameFile, (exists) => {
          if (!exists) {
            return console.error('Missing docker hostname, can\'t determine service restart owner');
          }

          readFile(hostnameFile, (err, data) => {
            const hostname = data.toString().trim();
            const lowestInstance = instances[0];
            
            if (hostname === lowestInstance.node) {
              console.log(`Identified service restart owner: ${lowestInstance.name} running on ${lowestInstance.node}`);
              const shouldUpdateServiceByImage = repo.env && repo.env.UPDATE_SERVICE_BY_IMAGE;
              if (shouldUpdateServiceByImage && hash) {
                exec(`docker service update --image=${shouldUpdateServiceByImage}:${hash} ${repo.serviceId}`, (err, stdout, stderr) => {
                  if (err) {
                    console.error('Error restarting', err);
                  }
                  if (stderr) {
                    return console.error('STDERR >>>', stderr);        
                  }
                  
                  console.log(`[${repo.name || repo.workdir}] Restarted - `, stdout);
                  if (process.env.HOOKUP_POST_RESTART_COMMAND) {
                    exec(process.env.HOOKUP_POST_RESTART_COMMAND);
                  }
                });
              }
              else {
                exec(`docker service update --force ${repo.serviceId} --update-parallelism 1 --update-delay 30s`, (err, stdout, stderr) => {
                  if (err) {
                    console.error('Error restarting', err);
                  }
                  if (stderr) {
                    return console.error('STDERR >>>', stderr);        
                  }
                  
                  console.log(`[${repo.name || repo.workdir}] Restarted - `, stdout);
                  if (process.env.HOOKUP_POST_RESTART_COMMAND) {
                    exec(process.env.HOOKUP_POST_RESTART_COMMAND);
                  }
                });
              }
            }
            else {
              console.log(`Not restart owner (${lowestInstance.name}) skipping service restart`);
            }
          });
        });
      });      
    };

    const restartContainerOrService = () => {
      if (repo.serviceId) {
        restartService();
      }
      else {
        restart();
      }
    };

    const nginxConfigPath = path.join(normalizedDeployPath, '.nginx');
    const virtualHosts = (repo.env && repo.env.VIRTUAL_HOST && repo.env.VIRTUAL_HOST.split(',')) || [];
    exists(nginxConfigPath, (exists) => {
      let promises = [];
      if (exists) {
        promises = virtualHosts.map((vhost) => {
          return new Promise((resolve, reject) => {
            console.log(`Trying to copy ${vhost} nginx config file to nginx vhost.d folder`);
            
            tryExecSync(`cp ${nginxConfigPath}/${vhost} /vhosts`);
            tryExecSync(`cp ${nginxConfigPath}/${vhost}.conf /vhosts`);
            return resolve();            
          });
        });
      }

      return Promise.all(promises).then(restartContainerOrService);
    });
    
  });
};
